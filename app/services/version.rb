# frozen_string_literal: true

class Version
  VERSION = "0.26.1"
end
